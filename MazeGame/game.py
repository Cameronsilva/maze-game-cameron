import csv
from itertools import count
import black
import matplotlib.pyplot as plt
from fileinput import filename
import pygame, sys
from sprites import Player, Wall, Point
from block_data import get_all

black = (0, 0, 0)
white = (255, 255, 255)
blue = (0, 0, 255)

pygame.init()

get_all = list(csv.reader(open('mazedb.csv',"r")))[1:]

screen = pygame.display.set_mode([1050, 1050])

pygame.display.set_caption('Maze')

background = pygame.Surface(screen.get_size())

background.fill(black)


player = Player(120, 120)
all_sprite_list = pygame.sprite.Group()
point_list = pygame.sprite.Group()
all_sprite_list.add(player)
wall_list = pygame.sprite.Group()
point_hit_list = pygame.sprite.Group()

for cells in get_all:
    first_cell_data = cells
    cell_name = first_cell_data[0] # 0th elemnet of 0th cell
    cell_east = first_cell_data[1]
    cell_west = first_cell_data[2]
    cell_north = first_cell_data[3]
    cell_south = first_cell_data[4]
    row = int(cell_name.split(",")[0].lstrip('(')) # extract y position from cell name (row!)
    col = int(cell_name.split(",")[1].rstrip(')')) # extract x position (column!)
    wall_east = Wall((col+1)*80, row*80, 5, 80)
    wall_west = Wall(col*80, row*80, 5, 80)
    wall_north = Wall(col*80, row*80, 80, 5)
    wall_south = Wall(col*80, (row+1)*80, 80, 5)
    points = Point((col+0.5)*80, (row+0.5)*80, 20, 20)
    if cell_east == "1":
        wall_list.add(wall_east)
    if cell_west == "1":
        wall_list.add(wall_west)
    if cell_north == "1":
        wall_list.add(wall_north)
    if cell_south == "1":
        wall_list.add(wall_south)
    point_list.add(points)

    
done = False

clock = pygame.time.Clock()

score_value = 0

while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                player.changespeed(-3, 0)
            elif event.key == pygame.K_RIGHT:
                player.changespeed(3, 0)
            elif event.key == pygame.K_DOWN:
                player.changespeed(0, 3)
            elif event.key == pygame.K_UP:
                player.changespeed(0, -3)

        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT:
                player.changespeed(3, 0)
            elif event.key == pygame.K_RIGHT:
                player.changespeed(-3, 0)
            elif event.key == pygame.K_DOWN:
                player.changespeed(0, -3)
            elif event.key == pygame.K_UP:
                player.changespeed(0, 3)

    player.update(wall_list)

    point_list.update(all_sprite_list)

    screen.fill(black)   

    all_sprite_list.draw(screen)

    point_list.draw(screen)

    timer = pygame.time.get_ticks()

    TIME = timer/1000
    
    wall_list.draw(screen)

    screen.blit((pygame.font.SysFont("Arial Bold", 30).render("Time: "+str(round(TIME))+"secs", False, (255, 255, 255))),(200, 20))

    ##screen.blit((pygame.font.SysFont("Arial Bold", 30).render("Score: "+str(score_value), False, (255, 255, 255))),(400, 20))

    pygame.display.flip()

    clock.tick(60)

pygame.quit()





