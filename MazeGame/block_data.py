import csv
from fileinput import filename
from importlib.resources import path

cells = list(csv.reader(open('mazedb.csv',"r")))[1:]
first_cell_data = cells
cell_name = first_cell_data[0] # 0th elemnet of 0th cell
cell_east = first_cell_data[1]
cell_west = first_cell_data[2]
cell_north = first_cell_data[3]
cell_south = first_cell_data[4]

def get_all():
    return cells

"""
class cellData:
    def __init__(self, filename):
        cells = list(csv.reader(open('mazedb.csv',"r")))[1:]
        first_cell_data = cells
    def cell_name(self):
        cell_name = first_cell_data[0]
        return cell_name
    def cell_east(self):
        cell_east = first_cell_data[1]
        return cell_east    """
