from tkinter import Y
import pygame

black = (0, 0, 0)
white = (255, 255, 255)
blue = (0, 0, 255)
red = (255, 0, 0)
gold = (255,215,0)



class Wall(pygame.sprite.Sprite):
    def __init__(self, x, y, width, height):
        pygame.sprite.Sprite.__init__(self)

        self.image = pygame.Surface([width, height])
        self.image.fill(blue)

        self.rect = self.image.get_rect()
        self.rect.y = y
        self.rect.x = x

class Player(pygame.sprite.Sprite):

    change_x = 0
    change_y = 0

    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)

        self.image = pygame.Surface([50, 50])
        self.image.fill(white)

        self.rect = self.image.get_rect()
        self.rect.x += x
        self.rect.y += y


    def changespeed(self, x, y):
        self.change_x += x
        self.change_y += y

    def update(self, walls):

        self.rect.x += self.change_x

        block_hit_list = pygame.sprite.spritecollide(self, walls, False)
        for block in block_hit_list:
            if self.change_x > 0:
                self.rect.right = block.rect.left
            else:
                self.rect.left = block.rect.right

        self.rect.y += self.change_y
        block_hit_list = pygame.sprite.spritecollide(self, walls, False)
        for block in block_hit_list:
            if self.change_y > 0:
                self.rect.bottom = block.rect.top
            else:
                self.rect.top = block.rect.bottom

class Point(pygame.sprite.Sprite):

    change_x = 0
    change_y = 0
            
    def __init__(self, x, y, width, height):
        pygame.sprite.Sprite.__init__(self)

        self.image = pygame.Surface([15, 15])
        self.image.fill(gold)

        self.rect = self.image.get_rect()
        self.rect.x += x
        self.rect.y += y

        self.count = 0

    def update(self, Player):

        self.rect.x += self.change_x

        point_hit_list = pygame.sprite.spritecollide(self, Player, False)
        for points in point_hit_list:
            pygame.sprite.Sprite.kill(self)
            self.count += 100
            print("killed")
            print(self.count)
        
        
        
        


        




        
        




        
            
            


    
    

